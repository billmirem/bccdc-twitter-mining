import twitter
from datetime import datetime as dt
from util import Query, loadTermsFromCSV
    
#########################################################################
### Set variables

# Directory location of output CSV - make sure directory exists!
output_file = 'outputs/my_output.csv'

# OPTIONAL: automatically tag the date/time to the output file name:
#date = dt.now().strftime('%Y-%m-%d_%H%M')
#output_file = 'outputs/output_{0}.csv'.format(date)

# OPTIONAL: provide CSV name for NLTK tokenizer output (also uncomment code below):
#nltk_output_file = 'outputs/nltk_output.csv'

# Directory location of CSV containing "keywords" (i.e. primary search
# terms)
keyword_file = 'inputs/keywords.csv'

# Directory location of CSV containing secondary search terms
secondary_file = 'inputs/secondary.csv'

# Directory location of a geojson text file representing the Health Areas,
# used to spatially filter the results (and tag to the appropriate HA)
health_areas_geojson = 'inputs/BC_healthareas.json'

# Set authentication parameters for connecting to the Twitter API
OAUTH_TOKEN = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
OAUTH_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
CONSUMER_KEY =  'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
CONSUMER_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

#########################################################################
### Prepare the program

# Create Twitter API authentication object
auth=twitter.oauth.OAuth(OAUTH_TOKEN,
                         OAUTH_SECRET,
                         CONSUMER_KEY,
                         CONSUMER_SECRET)

#########################################################################
### Run the search and filter and parse the results

statuses = Query(auth,
                 loadTermsFromCSV(keyword_file),
                 loadTermsFromCSV(secondary_file)
                 ).execute()

statuses.spatiallyFilterByGeoJSON(health_areas_geojson)

statuses.parseToCSV(output_file)

# OPTIONAL: For NLTK tokenizer output, uncomment this code:
#statuses.extractTokensToCSV(nltk_output_file)
