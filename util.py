import twitter
import time
from shapely.geometry import shape, Point
import json
import re
        

def loadTermsFromCSV(csv):
    '''
        Loads a list of search terms from a CSV.
    '''
    in_file = open(csv, "r")
    lines = in_file.readlines()
    in_file.close()
    
    terms = {}

    hdr = lines[0].rstrip('\n').rstrip('\r').split(',')
    for line in lines[1:]:
        ln = line.rstrip('\n').rstrip('\r').split(',')
        term = ln[0]
        
        g = False
        if 'generic' in hdr:
            g = bool(int(ln[hdr.index('generic')]))
        
        # Add quotes if the term is multiple words
        if len(term.split()) > 1:
            term = '"' + term + '"'
            
        terms[term] = {'generic': g} 

    return terms


class Query():
    '''
        Object for parameterizing and executing Twitter GET search/tweets
        API. Initialize with twitter.auth and a set of primary and
        secondary search terms. Optionally initialize with a geocode parameter
        (default will search centroid of British Columbia bounding box and 
        1500km).
    '''
    def __init__(self,
                 auth = None,
                 terms_primary = {},
                 terms_secondary= {},
                 geocode = None):
        
        # Establish connection with the Twitter API
        self.twitter_api = twitter.Twitter(auth=auth)
        
        # Set static search parameters
        self.search_args = {
               'count': 100,
               'result_type': 'recent'
               }
        
        # These are the geocode params to use <- can set to multiple if desired
        if not geocode:
            self.geocode = {
                'xys' : [(-126.65,54.15)],
                'radius' : '1500km'
                }
        
        self.terms_primary = terms_primary
        self.terms_secondary = terms_secondary
    
    def _getTweets(self):
        '''
            Retrieves Twitter results from a single GET query using the
            Search Tweets API. Includes handling of 'Rate limit exceeded'
            error, which, when received tells the application to wait 15 minutes
            and try again.
                            
            Returns:
                twitter_api.search.tweets() outputs
        '''
        try:
            return self.twitter_api.search.tweets(**self.search_args)
    
        except twitter.TwitterHTTPError, e:
            # If we've exceed the rate limit, wait 15 minutes for the limit to reset and continue
            if e.response_data.split('"message":"')[1].split('"')[0] == 'Rate limit exceeded':
                # Send alert
                now = time.strftime('%H:%M:%S', time.localtime())
                print ('***Twitter API rate limit exceeded***'
                    '\nTime that limit was exceeded: {0}\n'.format(now))
                
                # And wait until rate limit is reset...
                for i in range(15):
                    print 'Continuing in {0} minutes...'.format(15 - i)
                    time.sleep(60)
                
                return self._getTweets()
            else:
                raise


    def _pageThroughTwitterResults(self, results, statuses):
        '''
            Pages through all Tweets (up to the API-imposed 7-day limit)
            returned by the query parameters in the given "results"
            argument.
            
            Args:
            
                results:    dict representing the output of
                            twitter_api.search.tweets()
                            
                statuses:   list of twitter_api.search.tweets()['statuses']
            
            Returns:
                A list of twitter_api.search.tweets()['statuses']
        '''
        if len(results['statuses']) > 0:
            # Add max_id param indicating to ignore any Tweet already fetched
            max_id = results['statuses'][-1]['id']
            self.search_args['max_id'] = max_id - 1
            
            # Get next set of results
            results = self._getTweets()
                
            statuses += results['statuses']
            
            return self._pageThroughTwitterResults(results, statuses)
        
        else:
            return statuses
        
    def _processTwitterQuery(self, query):
        '''
            Handles post-processing of a compiled query parameter
            (i.e. the 'q' parameter for the Twitter API). Executes the search
            and returns a list of statuses once the query parameter has been
            scrubbed.
            
            Args:
            
                query:  string representing input for the 'q' Twitter GET
                        search/tweets API, e.g. "wut AND guy OR foo OR bar"
            
            Returns:
                A list of twitter_api.search.tweets()['statuses']
        '''
        query = query.rstrip(' OR ')
        self.search_args['q'] = query
        
        if 'max_id' in self.search_args:
            del self.search_args['max_id']
            
        results = self._getTweets()
        
        return self._pageThroughTwitterResults(results, results['statuses'])


    def execute(self):
        '''
            Builds a series of Twitter Search API 'q' parameters,
            executes the searches, and returns the results.
            
            Returns:
                Statuses() object
        '''
        print 'Starting search...'
        
        # This variable holds all the Tweets collected
        statusesAll = {}
        
        # For each of the coordinate pairs listed in the geocode parameter...
        for xy in self.geocode['xys']:
            print 'Querying {2} around: [{0},{1}]'.format(xy[1],
                                                        xy[0],
                                                        self.geocode['radius'])
            
            statuses = []
            self.search_args['geocode'] = '{0},{1},{2}'.format(xy[1],
                                                        xy[0],
                                                        self.geocode['radius'])

            # Build 'q' paramter for run queries for terms requiring a
            # secondary term
            terms = {k:v for k,v in self.terms_primary.items()
                        if v['generic']}
            for term1 in sorted(terms):
                query = term1 +  ' AND '
                for term2 in self.terms_secondary:
                    if (not (self.terms_primary[term1]['generic'] and 
                             self.terms_secondary[term2]['generic'])
                        and term1 <> term2):
                        # There is a limit of ~500 characters for queries...
                        # If the query is less than 400 characters, keep
                        # appending to the original query
                        if (len(query) + len(term2) + 4) < 400:
                            query += (term2 + ' OR ')
                            
                        # If adding the secondary term would create a query
                        # > 400 characters, execute and reset the query
                        else:
                            statuses += self._processTwitterQuery(query)
                            query = term1 + ' AND ' + term2 + ' OR '
                        
                # Execute the query on the remaining query parameter
                statuses += self._processTwitterQuery(query) 

            # Build 'q' parameter and run queries for terms not requiring a 
            # secondary term
            terms = {k:v for k,v in self.terms_primary.items()
                        if not v['generic']}
            query = ''
            for term1 in sorted(terms):
                if (len(query) + len(term1) + 4) < 400:
                    query += (term1 + ' OR ')
                    
                else:
                    statuses += self._processTwitterQuery(query)
                    query = term1 + ' OR '
        
            # Execute the query on the remaining query parameter
            statuses += self._processTwitterQuery(query) 
            
            # Storing results in a dict w/ status id as key prevents duplicates
            for status in statuses:
                if status['id'] not in statusesAll:
                    statusesAll[status['id']] = status
        
        return Statuses(statusesAll,
                        self.terms_primary.keys() + self.terms_secondary.keys())
    

class Statuses():
    '''
        Object for holding/parsing Twitter status search results.
    '''
    def __init__(self, all={}, terms=[]):
        '''
                
            Args:
                all:        dictionary of Twitter statuses collected from the
                            Twitter GET search/tweets API, formatted like so:
                            {'<statusid1>': {<status dict1>},
                             '<statusid2>': {<status dict2>},
                             <etc...>}
                             
                terms:      list of search terms that used to filter the
                            statuses. Used to identify the terms by which
                            statuses were tagged.
        '''
        self.all = all
        self.terms = list(terms)
        
        # Intake processing of the statuses
        for s in self.all:
            # Add an entry for each status that represents the text cleaned of
            # special characters
            
            status = self.all[s]
#            cleaned_text = re.sub(r'([^\s\w]|_)+', '', status['text']).lower()
            cleaned_text = re.sub(r'([^\s\w]|_)+', ' ', status['text']).lower()
            self.all[s]['cleaned_text'] = cleaned_text
            
            # Extract the search terms found in the status
            kws = []
            for term in self.terms:
                t = term.lower().replace('"','')
                
                if (t in cleaned_text.split() or
                    (len(t.split())>1 and t in cleaned_text)
                    ) and t.lower() not in kws:
                    kws.append(t.lower())
            self.all[s]['terms'] = kws
            
        
    def __repr__(self):
        return self.all
    
 
    def spatiallyFilterByGeoJSON(self, json_file):
        '''
            Removes all statuses w/ coordinates that are not w/in the geometry
            specified in the GeoJSON contained in the input file 
        
            Args:
            
                json_file:  directory location of an GeoJSON data used for
                            spatial filtering and tagging (i.e. only statuses
                            w/ coordinates w/in the GeoJSON geometry will be
                            loaded)
        
        '''
        
        # Load the GeoJSON file
        health_areas = json.loads(open(json_file, 'r').read())
        
        new =  {}
        for s in self.all:
            status = self.all[s]
            
            # Spatially filter
            try:
                status['geo']['coordinates'][0]
            except TypeError:
                continue
            else:
                x = status['geo']['coordinates'][1]
                y = status['geo']['coordinates'][0]
                pnt = Point([x,y])
                
                # Get BC Health Area number/name 
                self.all[s]['ha_num'] = None
                self.all[s]['ha_name'] = None
                for feat in health_areas['features']:
                    if shape(feat['geometry']).contains(pnt):
                        self.all[s]['ha_num'] = feat['properties']['ha_num']
                        self.all[s]['ha_name'] = feat['properties']['ha_name']
                
                if self.all[s]['ha_num'] and self.all[s]['ha_name']:
                    new[s] = status
        
        self.all = new
                
                
    def extractTokensToCSV(self, nltk_output_file):
        '''
            Outputs CSV of tokens (i.e. ANY word occurring in a status), summarized
            by user and health area.
        
            Args:
                
                nltk_output_file: directory location of the nltk output csv file  
        '''
        
        # Importing here so that users aren't required to have nltk installed
        # to use basic functionality
        from nltk.corpus import stopwords
        import nltk
        
        # Open the NLTK output file and write header
        nltk_hdr = ['TOKEN', 'COUNT', 'USERS', 'HA_NAME']
        nltk_out = open(nltk_output_file, 'w')
        nltk_out.write(','.join(nltk_hdr) + '\n') #header information

        # corpus dict format:
        # {token:[{count:INT},{STR:INT},{STR:INT, STR:INT, STR:INT, STR:INT, STR:INT}]}
        # token   count       user:count   HA:count
        corpus = {}
        for s in sorted(self.all):
            status = self.all[s]
            author = status['user']['screen_name']
            ha_name = status['ha_name']
            tokens = nltk.word_tokenize(status['cleaned_text'])
            
            # Filter out stop words
            ftokens = [x for x in tokens if x not in stopwords.words('english')]
            
            # Add filtered tokens to corpus dictionary
            for f in ftokens:
                if f in corpus: # check if token already exists
                    corpus[f][0]['count'] += 1
                    if author in corpus[f][1]: # check if author has used this token before
                        corpus[f][1][author] += 1
                    else: # if not then create record
                        corpus[f][1][author] = 1

                    if ha_name in corpus[f][2]: # check token has been used in this health area before
                        corpus[f][2][ha_name] += 1
                    else: # if not then create record
                        corpus[f][2][ha_name] = 1

                else: # create record for new tokens
                    corpus[f] = [{'count':1},{status['user']['screen_name']:1},{ha_name:1}]
        
        # Write outputs, sorting w/ highest frequency tokens first
        for c in sorted(corpus.items(), key=lambda x: x[1][0]['count'], reverse=True):
            nltk_out.write('"{0}","{1}","{2}","{3}"\n'.format(c[0],
                                                str(c[1][0]['count']),
                                                str(c[1][1]),
                                                str(c[1][2])))

        nltk_out.close()

        print '{0} terms archived in {1}'.format(len(corpus), nltk_output_file)
        
        
    def parseToCSV(self, output_file):
        '''
            Parses a list of twitter.search.tweets()['statuses'] to CSV file.
            
            
            Args:

                output_file: directory location of the output csv file

        '''
        
        # Open the output file and write header
        hdr = ['TERM(S)','USER','DATE&TIME','TWEET',
            'LOCATION-TOWN','LOCATION-PROV', 'X','Y','HA_NUM','HA_NAME',
            'NUMB_RT','NUMB_FAV','URL']
        out = open(output_file, 'w')
        out.write(','.join(hdr) + '\n') #header information
        
        # Cycle through the statuses and parse out the data
        for s in sorted(self.all):
            status = self.all[s]
            
            # Parse out tweet info to write to CSV
            sn = status['user']['screen_name']
            place = status['place']['full_name']
            place = place.split(',') if ',' in place else [place,'']
            
            text = status['text'].\
                    encode('ascii','ignore').\
                    replace('"',"'").\
                    replace('\r','').\
                    replace('\n','')
            
            data = ['|'.join(status['terms']),
                    sn,
                    status['created_at'],
                    '"{0}"'.format(text),
                    place[0],
                    place[1],
                    str(status['geo']['coordinates'][1]),
                    str(status['geo']['coordinates'][0]),
                    str(status['ha_num']),
                    status['ha_name'],
                    str(status['retweet_count']),
                    str(status['favorite_count']),
                    'https://twitter.com/{0}/status/{1}'.\
                        format(sn,status['id'])]
            
            out.write(','.join(x.encode('ascii','ignore') for x in data) + '\n')

        out.close()

        print '{0} statuses written to file: {1}'.format(len(self.all.keys()), output_file)
