BCCDC Twitter Mining
====================

A customized Python implementation of the [Twitter GET search/tweets API](https://dev.twitter.com/rest/reference/get/search/tweets) developed for the [British Columbia Centre for Disease Control](http://www.bccdc.ca/default.htm). The code collects the last 7 days of geocoded tweets occurring within British Columbia that contain some combination of public health-related primary and secondary search terms. Results can be exported to CSV.

**Table of Contents**

- [Requirements](#markdown-header-requirements)
- [Installation (Linux)](#markdown-header-installation-linux)
- [Installation (Windows)](#markdown-header-installation-windows)
    - [Install Python Setuptools](#markdown-header-install-python-setuptools)
    - [Install Shapely and GEOS dependencies](#markdown-header-install-shapely-and-geos-dependencies)
    - [Install the package](#markdown-header-install-the-package)
- [Optional installation: Python Natural Language Toolkit](#markdown-header-optional-installation-python-natural-language-toolkit)
- [Running the Search](#markdown-header-running-the-search)
- [How the Search Works](#markdown-header-how-the-search-works)
    - [Spatial Search](#markdown-header-spatial-search)
    - [Keyword Search](#markdown-header-keyword-search)
    - [CSV Output](#markdown-header-csv-output)

- - - -

## Requirements ##
- [Python 2.7](https://www.python.org/downloads/)
- [Shapely](https://pypi.python.org/pypi/Shapely#downloads)
- [GEOS](http://trac.osgeo.org/geos/)
- [Minimalist Twitter API for Python](https://github.com/sixohsix/twitter)
- [Twitter app OAuth and Consumer tokens](https://dev.twitter.com/oauth/overview/application-owner-access-tokens)

- - - -

## Installation (Linux) ##

```````````````````````````````````````
#!bash

$ python setup.py install
```````````````````````````````````````

- - - -

## Installation (Windows) ##

### Install Python Setuptools (if you don't already have them) ###

* Download (right-click and choose **Save link as...**): [get-pip.py](https://raw.github.com/pypa/pip/master/contrib/get-pip.py)
* Run it from command prompt:

```````````````````````````````````````
#!bash

$ cd C:\\Users\\Me\\Downloads\\
$ get-pip.py
```````````````````````````````````````
### Install Shapely and GEOS dependencies ###

* Download the [Shapely .whl file appropriate for your Python version](http://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely), e.g. if your Python is 32-bit v2.7, select this file:

```````````````````````````````````````
#!bash
Shapely‑1.5.7‑cp27‑none‑win32.whl

```````````````````````````````````````

* Run this command to install the **.whl** file (substitute name/location of your **pip.exe** and **Shapely .whl** files as necessary), e.g.:
    
```````````````````````````````````````
#!bash

$ C:\\Python27\\Scripts\\pip.exe install C:\\Users\\Me\\Downloads\\Shapely-1.5.7-cp27-none-win32.whl
```````````````````````````````````````

### Install the package ###
[Download](https://bitbucket.org/billmirem/bccdc-twitter-mining/downloads) and unzip the repository code and run the **setup.py** file, e.g.

```````````````````````````````````````
#!bash

$ cd C:\\Users\\Me\\MyCode\\bccdc-twitter-mining\\
$ setup.py install
```````````````````````````````````````
All done! 

## Optional installation: Python Natural Language Toolkit ##
[NLTK](http://www.nltk.org/index.html) is a Python module for working with human language data. Our package has an optional data parsing function that takes advantage of this toolkit: **Statuses.extractTokenstoCSV** summarizes the frequency of every non-stopword used in every status by both user and health area. To use this function, following the NLTK installation instructions below:

```````````````````````````````````````
#!bash

# Linux - use pip or easy_install:
$ sudo easy_install nltk

# Windows - use pip:
$ C:\\Python27\\Scripts\\pip.exe install nltk
```````````````````````````````````````
Once installed, in either OS open a **Python shell** and run the NLTK download tool:

```````````````````````````````````````
#!python

import nltk
nltk.download()

```````````````````````````````````````
A GUI will open. Within the GUI, select and download these packages:
```````````````````````````````````````
#!bash
Corpora | stopwords
Models | punkt
```````````````````````````````````````
All done!

- - - -

## Running the Search ##

The **run.py** file is a simple wrapper you can use to run the Twitter search and parse the results to **CSV** all in one command, like this:

```````````````````````````````````````
#!bash

# Linux
$ python run.py

# Windows
$ run.py
```````````````````````````````````````

At the top of **run.py**, program variables are set.  Using any text editor, modify as needed to specify your output file (ensure the directory location exists first), use different keyword and/or secondary search term .CSVs, and to specify [your Twitter API authentication tokens](https://dev.twitter.com/oauth/overview/application-owner-access-tokens). The contents of **run.py** are shown below:

```````````````````````````````````````
#!python

import twitter
from datetime import datetime as dt
from util import Query, loadTermsFromCSV
    
#########################################################################
### Set variables

# Directory location of output CSV - make sure directory exists!
output_file = 'outputs/my_output.csv'

# OPTIONAL: automatically tag the date/time to the output file name:
#date = dt.now().strftime('%Y-%m-%d_%H%M')
#output_file = 'outputs/output_{0}.csv'.format(date)

# OPTIONAL: provide CSV name for NLTK tokenizer output (also uncomment code below):
#nltk_output_file = 'outputs/nltk_output.csv'

# Directory location of CSV containing "keywords" (i.e. primary search
# terms)
keyword_file = 'inputs/keywords.csv'

# Directory location of CSV containing secondary search terms
secondary_file = 'inputs/secondary.csv'

# Directory location of a geojson text file representing the Health Areas,
# used to spatially filter the results (and tag to the appropriate HA)
health_areas_geojson = 'inputs/BC_healthareas.json'

# Set authentication parameters for connecting to the Twitter API
OAUTH_TOKEN = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
OAUTH_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
CONSUMER_KEY =  'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
CONSUMER_SECRET = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

#########################################################################
### Prepare the program

# Create Twitter API authentication object
auth=twitter.oauth.OAuth(OAUTH_TOKEN,
                         OAUTH_SECRET,
                         CONSUMER_KEY,
                         CONSUMER_SECRET)

#########################################################################
### Run the search and filter and parse the results

statuses = Query(auth,
                 loadTermsFromCSV(keyword_file),
                 loadTermsFromCSV(secondary_file)
                 ).execute()

statuses.spatiallyFilterByGeoJSON(health_areas_geojson)

statuses.parseToCSV(output_file)

# OPTIONAL: For NLTK tokenizer output, uncomment this code:
#statuses.extractTokensToCSV(nltk_output_file)


```````````````````````````````````````

- - - -

## How the Search Works ##

The search uses the [Twitter GET search/tweets Search API](https://dev.twitter.com/rest/reference/get/search/tweets) to retrieve all statuses in the past 7 days the meet the query parameters.


### Spatial Search ###
For the **geocode** parameter, we use the centroid of the British Columbia boundary box and a 1500 km radius, which captures the area shown below:

![ss_BC_CDC_AOI3.png](https://bitbucket.org/repo/B8LB8L/images/785334786-ss_BC_CDC_AOI3.png)

Because this search area covers large amounts of land outside of BC, we spatially filter the results using the **inputs\BC_healthareas.json** file. During this step, each status is also tagged with the **health area** that it falls into. Health area delineations are shown below:

![ss_BC_CDC_AOI5_has.png](https://bitbucket.org/repo/B8LB8L/images/888684969-ss_BC_CDC_AOI5_has.png)

### Keyword Search ###
Within the spatial area described above, we search for statuses containing certain combinations of public health-related keywords. The **keywords.csv** lists the **primary** search terms, e.g.:

| term      |generic|
|------------|---------|
|acetaminophen| 0|
|ice|1|

The **generic** field indicates whether or not the term should be searched for on its own:

* **generic = 0**: include any status that contains the term
* **generic = 1**: include only statuses that contain the term *AND* at least one of the **secondary** search terms listed in **inputs/secondary.csv**.

### CSV Output ###
Results are parsed to a CSV containing these fields:

* **TERM(S)**: a list of all search terms by which the status was tagged
* **USER**: the screen name of the Twitter user
* **DATE&TIME**: the time the status was created
* **TWEET**: the status text
* **LOCATION-TOWN**: the place name associated with the status
* **LOCATION-PROV**: the place name associated with the status
* **X**: longitude
* **Y**: latitude
* **HA_NUM**: BC Health Area number
* **HA_NAME**: BC Health Area name
* **NUMB_RT**: the number of times the status has been retweeted
* **NUMB_FAV**: the number of times the status has been favorited
* **SOURCE**: device/application that the status was posted from
* **URL**: a URL linking to the status

[lexers]: http://pygments.org/docs/lexers/
[fireball]: http://daringfireball.net/projects/markdown/ 
[Pygments]: http://www.pygments.org/ 
[Extra]: http://michelf.ca/projects/php-markdown/extra/
[id]: http://example.com/  "Optional Title Here"
[BBmarkup]: https://confluence.atlassian.com/x/xTAvEw