import os
import platform
from setuptools import setup

# Utility function to read the README file, used for long_description (which is used for PyPI documentation)
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()        

# get list of python dependencies
with open('DEPENDENCIES.txt') as f:
    required = f.read().splitlines()

# Check if Linux and run setup.sh if so
if 'Linux' in platform.platform():
    os.system('chmod a+x ./setup.sh;bash ./setup.sh')
else:
    required.remove('Shapely==1.5.7')

###############################
# Run setuptools setup
print 'Running python setup...'
 

setup(
      name='bccdc-twitter-mining',
      version=0.01,
      author='Michael Billmire',
      author_email='billmirem@gmail.com',
      description='A customized Python implementation of the Twitter Search API',
      long_description=read('README.md'),
      url='https://bitbucket.org/billmirem/bccdc-twitter-mining/',
      license='No license/not allowed to use',
      packages=[], # modules are specified by module name, not filename
      classifiers=[
        'Programming Language :: Python',
        'License :: Other/Proprietary License',
        'Operating System :: Linux BSD',
        'Operating System :: Windows OS',
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        ],
      install_requires=required,
      )